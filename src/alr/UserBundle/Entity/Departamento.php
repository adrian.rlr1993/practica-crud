<?php

namespace alr\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Departamento
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Departamento
{
<<<<<<< HEAD
/**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="departamentos")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $usuario;

=======
>>>>>>> e069d15b70ed349a14a48a629f749cdb49b2d693
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nombre", type="string", length=30)
     */
    private $nombre;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Departamento
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
<<<<<<< HEAD

    /**
     * Set usuario
     *
     * @param \alr\UserBundle\Entity\Usuario $usuario
     * @return Departamento
     */
    public function setUsuario(\alr\UserBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \alr\UserBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
=======
>>>>>>> e069d15b70ed349a14a48a629f749cdb49b2d693
}
